import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public url = 'http://localhost:5000/add-configuration';
  constructor(
    private http: HttpClient
  ) {
  }

  public getHeader() {
    return new HttpHeaders({
      'Content-Type': 'application/json',
    });
  };

  public testing(): Observable<any> {
    return this.http.get(`http://localhost:5000/test`, { headers: this.getHeader(), observe: 'response' })
      .pipe(map(res => res.body));
  }

  public getTicketingData(type:string): Observable<any>{
    return this.http.get(`http://localhost:5000/ticketDataByDateRange?type=${type}`, { headers: this.getHeader(), observe: 'response' })
    .pipe(map(res => res.body));
  }
  
  public addDashboardConfig(data:any):Observable<any>{
     return this.http.post(this.url,data);
  }

}
