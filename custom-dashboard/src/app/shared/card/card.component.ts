import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { Chart } from 'chart.js/auto';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexStroke,
  ApexGrid
} from "ng-apexcharts";


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @ViewChild("chart") chart: any;
  @Input() showChart: boolean = false;
  @Input() cardTitle: string = "";
  @Input() metrics: string = "";
  @Input() chartOptions: any;
}
