import { Component } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
  selectedTab: any;
  updateForm = new FormGroup({
    titleLabel: new FormControl(),
    subTitleLabel:new FormControl(),
    xAxisLabel: new FormControl(),
    yAxisLabel: new FormControl(),
    start: new FormControl<Date | null>(null),
    end: new FormControl<Date | null>(null),
  });

}
