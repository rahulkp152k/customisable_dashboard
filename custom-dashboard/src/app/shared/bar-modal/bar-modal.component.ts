import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';

@Component({
  selector: 'app-bar-modal',
  templateUrl: './bar-modal.component.html',
  styleUrls: ['./bar-modal.component.scss']
})

export class BarModalComponent {
  currentTemplate:number = 0;
  @Output() selectedTab : EventEmitter<any> = new EventEmitter();
  constructor(
    public dialogRef: MatDialogRef<BarModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedTab.emit(tabChangeEvent.index)
    this.currentTemplate = tabChangeEvent.index;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

}
