import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarModalComponent } from './bar-modal.component';

describe('BarModalComponent', () => {
  let component: BarModalComponent;
  let fixture: ComponentFixture<BarModalComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BarModalComponent]
    });
    fixture = TestBed.createComponent(BarModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
