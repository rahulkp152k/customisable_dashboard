import { AfterViewInit, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Chart } from 'chart.js/auto';

@Component({
  selector: 'app-pie-doughnut-chart',
  templateUrl: './pie-doughnut-chart.component.html',
  styleUrls: ['./pie-doughnut-chart.component.scss']
})
export class PieDoughnutChartComponent implements OnChanges, AfterViewInit {
  public chart: any;
  @Input() id: string = "";
  @Input() type: any;
  @Input() labels: any;
  @Input() datasets: any;

  ngOnChanges(changes: SimpleChanges): void {
    if(this.id){
      if (changes['datasets'].currentValue) {
        this.chart.data.labels = this.labels;
        this.chart.data.datasets = this.datasets;
        this.chart.update();
      }
     }
  }

  ngAfterViewInit() {
    if (this.id) {
      this.chart = new Chart(this.id, {
        type: this.type,

        data: {
          labels: this.labels,
          datasets: this.datasets
        },
        options: {
          aspectRatio: 2.5
        }
      });
    }

  }
}

