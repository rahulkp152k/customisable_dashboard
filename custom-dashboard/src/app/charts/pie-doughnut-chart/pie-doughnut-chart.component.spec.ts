import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PieDoughnutChartComponent } from './pie-doughnut-chart.component';

describe('PieDoughnutChartComponent', () => {
  let component: PieDoughnutChartComponent;
  let fixture: ComponentFixture<PieDoughnutChartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PieDoughnutChartComponent]
    });
    fixture = TestBed.createComponent(PieDoughnutChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
