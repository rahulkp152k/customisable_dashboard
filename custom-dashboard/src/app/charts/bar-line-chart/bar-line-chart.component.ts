import { AfterViewInit, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Chart } from 'chart.js/auto';

@Component({
  selector: 'app-bar-line-chart',
  templateUrl: './bar-line-chart.component.html',
  styleUrls: ['./bar-line-chart.component.scss']
})
export class BarLineChartComponent implements AfterViewInit, OnChanges {
  public chart: any;
  @Input() id: string = "";
  @Input() type: any;
  @Input() chartData: any;

  constructor() { }


  ngOnChanges(changes: SimpleChanges): void {
    if (this.id) {
      if (changes['chartData'].currentValue) {
        console.log(this.chartData.option)
        this.chart.data.labels = this.chartData.labels;
        this.chart.data.datasets = this.chartData.dataset;
        this.chart.options = this.chartData.option;
        this.chart.update();
      }
    }

  }

  ngAfterViewInit() {
    if (this.id) {
      this.chart = new Chart(this.id, {
        type: this.type,
        data: {
          labels: this.chartData.labels,
          datasets: this.chartData.dataset
        },
        options: this.chartData.option
      });
    }

  }
}
