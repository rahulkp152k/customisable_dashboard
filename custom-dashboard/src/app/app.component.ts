import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DataService } from './data.service';
import { DisplayGrid, GridsterConfig, GridsterItem, GridType } from 'angular-gridster2';
import { ModalComponent } from './shared/modal/modal.component';
import { BarModalComponent } from './shared/bar-modal/bar-modal.component';
import singleBarChartData from "../assets/sampleData/singleBarChartData.json";
import multiBarChartData from "../assets/sampleData/multiBarChartData.json"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public dataConfig: any;
  title = 'custom-dashboard';
  chartid1: string = 'barChart';
  chartid2: string = 'lineChart';
  chartid3: string = 'pieChart';
  chartid4: string = 'doughnutChart';
  charttype1: any = 'bar';
  charttype2: any = 'line';
  charttype3: any = 'pie';
  charttype4: any = 'doughnut';
  chartlabels1: any = [
    "40",
    "50",
    "19",
    "60",
    "79",
    "80",
    "67"
  ];
  chartdatasets1: any = [
    {
      "label": "Test-1",
      "data": [
        "40",
        "50",
        "19",
        "60",
        "79",
        "80",
        "34"
      ],
      "backgroundColor": "orange"
    },
    {
      "label": "Test-2",
      "data": [
        "44",
        "5",
        "80",
        "80",
        "41",
        "32",
        "45"
      ],
      "backgroundColor": "green"
    }
  ];
  chartlabels2: any = [
    "Test-1",
    "Test-2",
    "Test-3",
    "Test-4",
    "Test-5"
  ];
  chartdatasets2: any = [
    {
      "data": ["600", "897", "572", "379", "492"],
      "backgroundColor": [
        "orange",
        "pink",
        "red",
        "green",
        "blue"
      ]
    }
  ];
  chartOptions: any = JSON.parse(JSON.stringify(singleBarChartData.options));;
  newchartlabel3: any;
  newchartdataset3: any;
  gridsterOptions: GridsterConfig;
  dashboard: Array<GridsterItem> = [];
  currentConfiguration: Array<GridsterItem> = [];

  constructor(
    public dataservice: DataService,
    public dialog: MatDialog
  ) {
    this.gridsterOptions = {
      gridType: GridType.Fixed,
      setGridSize: true,
      minCols: 1,
      maxCols: 6,
      minRows: 1,
      maxRows: 6,
      maxItemCols: 6,
      minItemCols: 1,
      maxItemRows: 6,
      minItemRows: 1,
      minItemArea: 1,
      defaultItemCols: 1,
      defaultItemRows: 1,
      fixedRowHeight: 150,
      mobileBreakpoint: 500,
      keepFixedWidthInMobile: false,
      keepFixedHeightInMobile: true,
      pushItems: true,
      scrollSensitivity: 10,
      scrollSpeed: 20,
      ignoreMarginInRow: false,
      draggable: {
        enabled: true,
        stop: (item, gridsterItem, event) => {
          // console.info('drag-end');
          console.info(gridsterItem);
          // console.info(event);
          this.currentConfiguration = [];
          gridsterItem.gridster.grid.forEach((gridItem) => {
            this.currentConfiguration.push(gridItem.item);
          })
          // console.log(this.currentConfiguration);

        }
      },
      resizable: {
        enabled: true,
        stop: function (item, gridsterItem, event) {
          console.info('resize-end');
          console.info(item);
          console.info(gridsterItem);
          console.info(event);
          //do position update
        }
      },
      swap: false,
      disablePushOnDrag: false,
      disablePushOnResize: false,
      pushDirections: { north: true, east: true, south: true, west: true },
      pushResizeItems: false,
      displayGrid: DisplayGrid.None,
      disableWindowResize: false,
      disableWarnings: true,
      scrollToNewItems: true,
      compactType: 'compactLeft&Up'
    };
  }

  ngOnInit(): void {
    this.dashboard = [
      { cols: 3, rows: 3, y: 0, x: 0, widget: "bar", data: { labels: this.chartlabels1, dataset: this.chartdatasets1, option: this.chartOptions } },
      { cols: 3, rows: 3, y: 0, x: 3, widget: "pie", data: { labels: this.chartlabels2, dataset: this.chartdatasets2,  option: this.chartOptions } },
      { cols: 3, rows: 3, y: 3, x: 0, widget: "line", data: { labels: this.chartlabels1, dataset: this.chartdatasets1,  option: this.chartOptions } },
      { cols: 3, rows: 3, y: 3, x: 3, widget: "donut", data: { labels: this.chartlabels2, dataset: this.chartdatasets2,  option: this.chartOptions } }
    ];
    // this.configapicall();
    this.dataapicall();
  }

  configapicall() {
    this.dataservice.testing().subscribe(
      (res) => {
        if (res?.data) {
          if (res.data[2]?.data) {
            this.dashboard = res.data[2].data;
          }
        }
      },
      (err) => {
        console.log(err)
      }
    );
  }

  dataapicall() {
    this.dataservice.testing().subscribe(
      (res) => {
        if (res?.data) {
          // this.chartlabels1 = res.data[0].labels;
          // this.chartdatasets1 = res.data[0].chartdatasets1;
          // this.chartlabels2 = res.data[1].labels;
          // this.chartdatasets2 = res.data[1].chartdatasets2;
        }
      },
      (err) => {
        console.log(err)
      }
    );
  }

  save() {
    this.dataConfig = {
      "name": "Dashboard Layout",
      "data": JSON.parse(JSON.stringify(this.currentConfiguration))
    }
    this.dataservice.addDashboardConfig(this.dataConfig).subscribe(
      (response) => {
        console.log('Server response:', response);
      },
      (error) => {
        console.error('Error:', error);
      }
    );
  }

  delete(index: number) {
    this.dashboard?.splice(index, 1);
    console.log(this.dashboard)
  }

  update(index: number, type: string) {
    console.log(index, type)

    const dialogRefUpdate = this.dialog.open(ModalComponent, {
      width: '100%'
    });

    dialogRefUpdate.afterClosed().subscribe(result => {
      console.log(result.value);

      this.dataservice.getTicketingData(type).subscribe(
        (res) => {
          if (res?.data) {
            let temp: any = [];
            temp.push(res.data[0].ticketData.opened)
            temp.push(res.data[0].ticketData.inProgress)
            temp.push(res.data[0].ticketData.onHold)
            temp.push(res.data[0].ticketData.completed)

            this.dashboard[index]['data'].dataset[0].data = temp;

            console.log("dataaa", this.dashboard[index]['data'].dataset);

            let optionsCopy = this.chartOptions;

            if (result.value.titleLabel != null) {
              optionsCopy.plugins.title.display = true;
              optionsCopy.plugins.title.text = result.value.titleLabel;
            }

            if (result.value.subTitleLabel != null) {
              optionsCopy.plugins.subtitle.display = true;
              optionsCopy.plugins.subtitle.text = result.value.subTitleLabel;
            }

            if (result.value.xAxisLabel != null) {
              optionsCopy.scales.x.title.display = true;
              optionsCopy.scales.x.title.text = result.value.xAxisLabel;
            }

            if (result.value.yAxisLabel != null) {
              optionsCopy.scales.y.title.display = true;
              optionsCopy.scales.y.title.text = result.value.yAxisLabel;
            }

            this.dashboard[index]['data'].option = optionsCopy;

            console.log("while updating ", optionsCopy)


            //      let tempData1 = {
            //       labels : this.dashboard[index]['data'].labels,
            //       dataset: sampledata
            //      } 

            //      this.dashboard[index]['data'] = tempData1

            //      let tempData: any = {
            //       labels : multiBarChartData.labels,
            //       dataset: multiBarChartData.datasets
            // } 
            // this.dashboard[index]['data'].dataset[0].data = tempData.dataset[0].data;

            // console.log(tempData.dataset[0].data)
            // console.log("dddd json data", tempData);
            // console.log("dddd api data", tempData1);
            // this.dashboard[index]['data'] = tempData;
          }
        },
        (err) => {
          console.log(err)
        }
      );
    })

  }

  makeChart() {

    const dialogRef = this.dialog.open(BarModalComponent, {
      width: '100%',
      data: {
        chartData: singleBarChartData
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 0) {
        this.chartlabels1 = JSON.parse(JSON.stringify(singleBarChartData.labels));
        this.chartdatasets1 = JSON.parse(JSON.stringify(singleBarChartData.datasets));
        this.addItem('bar', { labels: this.chartlabels1, dataset: this.chartdatasets1, option: this.chartOptions }, 'singlebar');
      }
      else if (result == 1) {
        this.chartlabels1 = JSON.parse(JSON.stringify(multiBarChartData.labels));
        this.chartdatasets1 = JSON.parse(JSON.stringify(multiBarChartData.datasets));
        this.addItem('bar', { labels: this.chartlabels1, dataset: this.chartdatasets1, option: this.chartOptions }, 'multibar');
      }
    });

    // let activetab = 0;

    // const sub = dialogRef.componentInstance.selectedTab.subscribe((currentTab) => {
    //   activetab = currentTab;
    //   console.log(activetab)
    // });

  }

  addItem(itemType: string, data: any, type: string) {
    this.changeGridLayout();
    const obj = this.findMaxRow();
    this.dashboard?.push({ cols: 3, rows: 3, y: obj.row, x: obj.col, widget: itemType, data: data, type: type });
  }

  changeGridLayout() {
    let row = 0;
    let col = 0;

    this.dashboard?.forEach(chart => {
      if (chart.y > row) {
        row = chart.y;
        col = chart.x;
      }
    });

    if (this.gridsterOptions.maxRows && this.gridsterOptions.maxItemRows) {
      if (this.gridsterOptions.maxRows <= (row + 3)) {
        this.gridsterOptions.maxRows += 3;
        this.gridsterOptions.maxItemRows += 3;
        this.changedOptions();
      }
    }
  }
  // function for finding the row and col for the new cell to get inserted
  findMaxRow() {
    let row = 0;
    let col = 0;
    this.dashboard?.forEach(chart => {
      if (chart.y > row)
        row = chart.y;
      col = chart.x;
    });

    col = (col + 3) % 6;

    if (col == 0) {
      row += 3;
    }

    return { row: row, col: col };
  }

  // whenever the gridster options are changed by us then this function must be called to update the gridster options
  changedOptions() {
    if (this.gridsterOptions.api && this.gridsterOptions.api.optionsChanged) {
      this.gridsterOptions.api.optionsChanged();
    }
  }

  chartOptions1: any = {
    series: [
      {
        name: "Visits",
        data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
      }
    ],
    chart: {
      height: 280,
      type: "bar",
      zoom: {
        enabled: false
      },
      toolbar: {
        show: false,
      }
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: "straight"
    },
    grid: {
      show: false
    },
    xaxis: {
      show: false
    },
    yaxis: {
      show: false
    },
  };

  chartOptions2: any = {
    series: [
      {
        name: "Type1",
        data: [31, 40, 28, 51, 42, 109, 100]
      },
      {
        name: "Type2",
        data: [11, 32, 45, 32, 34, 52, 41]
      }
    ],
    chart: {
      height: 280,
      type: "area",
      toolbar: {
        show: false,
      }
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: "smooth"
    },
    xaxis: {
      show: false
    },
    yaxis: {
      show: false
    },
    tooltip: {
      x: {
        format: "dd/MM/yy HH:mm"
      }
    }
  };


  chartOptions3: any = {
    series: [
      {
        name: "Visits",
        data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
      }
    ],
    chart: {
      height: 280,
      type: "line",
      zoom: {
        enabled: false
      },
      toolbar: {
        show: false,
      }
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: "straight"
    },
    grid: {
      show: false
    },
    xaxis: {
      show: false
    },
    yaxis: {
      show: false
    },
  };

  chartOptions4: any = {
    series: [44, 55, 13, 43, 22],
    chart: {
      width: 350,
      type: "donut"
    },
    labels: ["Team A", "Team B", "Team C", "Team D", "Team E"],
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: "bottom"
          }
        }
      }
    ]
  };

}
