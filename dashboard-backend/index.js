var express = require("express");
const bodyParser = require("body-parser");
var cors = require("cors");
const mongoose = require("mongoose");
const MongoClient = require('mongodb').MongoClient

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


mongoose.connect("mongodb+srv://dashboardPOC:dashboardPOC@cluster0.qx1ry4h.mongodb.net/", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log("Connected with the MongoDB Atlas Database");
    })
    .catch((error) => {
        console.log(error, "Not Connected with the Database ");
    });

const PORT = 5000;

app.get("/", (req, res) => {
    res.send("Dashboard Server is up and running");
});

app.get("/test", async (req, res) => {
    const client = new MongoClient("mongodb+srv://dashboardPOC:dashboardPOC@cluster0.qx1ry4h.mongodb.net/");

    await client.connect();

    const db = client.db("testData");

    const collection = db.collection("Chartdata");

    const data = await collection.find().toArray();

    res.status(200).json({ message: "Fetched Data Successfully", data: data });
})

app.post("/add-configuration", async (req, res) => {
    const client = new MongoClient("mongodb+srv://dashboardPOC:dashboardPOC@cluster0.qx1ry4h.mongodb.net/");

    const body = req.body;
    await client.connect();

    const db = client.db("testData");
    const collection = db.collection("Chartdata");
    const result = await collection.updateOne(
        { name: "Dashboard Layout" },
        { $set: { "data": body.data } },
        { upsert: true }
    );
    if (result) {
        res.status(201).json({ message: "Added Data Successfully" });
    }
    else {
        res.status(401).json({ message: "Getting Error" });
    }

})

app.get("/ticketDataByDateRange", async (req, res) => {
    const client = new MongoClient("mongodb+srv://dashboardPOC:dashboardPOC@cluster0.qx1ry4h.mongodb.net/");

    await client.connect();

    const db = client.db("TemplateData");

    const collection = db.collection("SampleData");
    console.log("***********************",req.query);
   
    let data = await collection.find({date: {
        $gte: new Date("2023-11-01T18:30:00.000Z"),
        $lte: new Date("2023-11-05T18:30:00.000Z")
    }}).toArray();
      
    if(req.query.type=='singlebar'){
        let totalOpened=0,totalInProgess=0,totalOnHold=0,totalCompleted=0;
        data.forEach((val)=>{
            totalOpened+=val.ticketData.opened;
            totalInProgess+=val.ticketData.inProgress;
            totalOnHold+=val.ticketData.onHold;
            totalCompleted+=val.ticketData.completed
        })
        data = [{
            "ticketData":{ opened: totalOpened, inProgress: totalInProgess, onHold: totalOnHold, 
                completed: totalCompleted }}]
    }
    else{
        console.log("multiple chart Data");
    }
//    console.log("data : ",data);
    res.status(200).json({ message: "Fetched Data Successfully", data: data });
})

app.post('/ticketData',async(req,res)=>{
    // {
    //     "ticketData":{
    //         "opened":22,
    //         "inProgress":11,
    //         "onHold":4,
    //         "completed":7
    //     },
    //     "date":"2023-11-05T14:30:00Z"
    //   }
    const client = new MongoClient("mongodb+srv://dashboardPOC:dashboardPOC@cluster0.qx1ry4h.mongodb.net/");

    await client.connect();

    const db = client.db("TemplateData");

    const collection = db.collection("SampleData");
    console.log(req.body);
    const data = await collection.insertMany(req.body)
   

    res.status(201).json({"status":"success"})
})

app.listen(PORT, () =>
    console.log(`Server Started and running on port ${PORT}`)
);

